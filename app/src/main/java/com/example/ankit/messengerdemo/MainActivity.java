package com.example.ankit.messengerdemo;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

    public Messenger mMessenger;
    public boolean isBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_send_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBound) {
                    Message message = Message.obtain();
                    Bundle bundle = new Bundle();
                    bundle.putString(MessageService.KEY_MESSAGE, "HELLO SERVICE. I AM ACTIVITY");
                    message.setData(bundle);
                    try {
                        mMessenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        bindService(new Intent(this, MessageService.class), new Connection(this), BIND_AUTO_CREATE);
    }

    public static class Connection implements ServiceConnection {

        WeakReference<Context> contextReference;

        public Connection(Context context) {
            contextReference = new WeakReference<>(context);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Context context = contextReference.get();
            if (context != null) {
                ((MainActivity) context).mMessenger = new Messenger(service);
                ((MainActivity) context).isBound = true;
                Toast.makeText(context, "Service Connected", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Context context = contextReference.get();
            if (context != null) {
                ((MainActivity) context).isBound = false;
                Toast.makeText(context, "Service Disconnected", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
