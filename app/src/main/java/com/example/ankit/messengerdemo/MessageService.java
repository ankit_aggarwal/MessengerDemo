package com.example.ankit.messengerdemo;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class MessageService extends Service {

    public Messenger mMessenger = new Messenger(new MessageHandler(this));

    public static final String KEY_MESSAGE = "KEY_MESSAGE";

    public MessageService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    public static class MessageHandler extends Handler {

        WeakReference<Context> contextReference;

        public MessageHandler(Context context) {
            contextReference = new WeakReference<>(context);
        }

        @Override
        public void handleMessage(Message msg) {
            if (contextReference.get() != null) {
                Toast.makeText(contextReference.get(), msg.getData().getString(KEY_MESSAGE), Toast.LENGTH_SHORT).show();
            }
        }
    }
}